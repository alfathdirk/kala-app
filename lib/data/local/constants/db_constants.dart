class DBConstants {
  DBConstants._();

  // Store Name
  static const String STORE_NAME = 'kalaapp';

  // DB Name
  static const DB_NAME = 'kalaapp.db';

  // Fields
  static const FIELD_ID = 'id';
}
